using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace serverTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RowingController : ControllerBase
    {

        [HttpGet("TrainigInfo")]
        public IActionResult getTrainigInfo()
        {

            var trainingSlots = new List<Models.DTO.TrainingDefinitionDTO>(){
                new Models.DTO.TrainingDefinitionDTO{
                    Id = 1,
                    Extras = "serie:1h",
                    Icon = "running",
                    Name = "Carrera",
                    Ud = "km"
                },
                new Models.DTO.TrainingDefinitionDTO{
                    Id = 2,
                    Extras = "serie:2x1000",
                    Icon = "rowing",
                    Name = "Remo",
                    Ud = "00:00"
                },
                new Models.DTO.TrainingDefinitionDTO{
                    Id = 3,
                    Extras = "serie:3",
                    Icon = "streaching",
                    Name = "Estiramientos",
                    Ud = "km"
                }

            };

            var person1 = new Models.DTO.PesonDTO()
            {
                Id = 1,
                Name = "David"
            };
            var person2 = new Models.DTO.PesonDTO()
            {
                Id = 2,
                Name = "Pedro"
            };

            var trainings = new List<Models.DTO.TrainingSlotDTO>(){
                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 1,
                    Person = person1,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[0],
                    TrainingDefinitionId = 1,
                    Value = "15"
                },
                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 1,
                    Person = person1,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[1],
                    TrainingDefinitionId = 2,
                    Value = "03:30"
                },
                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 1,
                    Person = person1,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[2],
                    TrainingDefinitionId = 3,
                    Value = "-"
                }                ,
                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 2,
                    Person = person2,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[0],
                    TrainingDefinitionId = 1,
                    Value = "15"
                },

                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 2,
                    Person = person2,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[1],
                    TrainingDefinitionId = 2,
                    Value = "3:35"
                },

                new Models.DTO.TrainingSlotDTO(){
                    PersonId = 2,
                    Person = person2,
                    Date = new DateTime(2021, 07, 25),
                    Notes = "",
                    TrainingDefinition = trainingSlots[2],
                    TrainingDefinitionId = 3,
                    Value = "-"
                },
            };




            return Ok(new
            {
                TrainingInfo = trainingSlots,
                TrainingValues = trainings
            });
        }

    }
}