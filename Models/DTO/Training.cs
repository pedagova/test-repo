namespace Models.DTO
{

    //Esta clase representa la informacion de una de las fases de un entrenamiento
    public class TrainingDefinitionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ud { get; set; }
        public string Icon { get; set; }

        //Informacion extra sobre el entrenamiento. Las distintas lineas de información estaran separadas por ';'
        public string Extras { get; set; }

    }
}