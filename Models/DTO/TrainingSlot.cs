using System;
namespace Models.DTO
{

    //Esta clase representa la informacion de una de las fases de un entrenamiento
    public class TrainingSlotDTO
    {
        public int PersonId { get; set; }
        public PesonDTO Person { get; set; }
        public DateTime Date { get; set; }
        public int TrainingDefinitionId { get; set; }
        public TrainingDefinitionDTO TrainingDefinition { get; set; }

        public string Value { get; set; }

        public string Notes { get; set; }
    }
}